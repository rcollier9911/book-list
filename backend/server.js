var mysql = require('mysql');
const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
const dotenv = require('dotenv');
dotenv.config();

var router = express.Router();

var pool = mysql.createPool({
    connectionLimit: 10,
    host: "localhost",
    user: process.env.USER,
    password: process.env.PASS,
    database: "book_database"
});

// routes for all books and adding books
router.route('/book')
.get(function (req, res){
    pool.getConnection(function(err, connection){
        if(err) throw err;
        const sql = "SELECT title, author FROM book";
        connection.query(sql, function(err, result, fields){
            if(err) throw err;
            res.status(200).json(result);
        });
        connection.release();
    });
});

// sanatize the input on the website AND here on the backend.
// routes for specific books, checking them or editing them etc.
router.route('/book/:title')
.get(function(req,res){
    const title = req.params.title
    console.log(title);
    // TODO: sanatize input function goes here
    pool.getConnection(function(err, connection){
        if(err) throw err;
        const sql = `SELECT title, author FROM book WHERE title = '${title}'`;
        connection.query(sql, function(err, result, fields){
            if(err) throw err;
            console.log(result);
            res.status(200).json(result);
        });
        connection.release();
    });
});
app.use('/api', router);
app.listen(process.env.PORT);
console.log(`server started on port ${process.env.PORT}`);
