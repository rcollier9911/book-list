import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from '../http-service.service';

import {CommonModule} from '@angular/common';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css'],
  providers: [HttpServiceService]
})
export class MainViewComponent implements OnInit {

books: Book[]; // put in a scrolling module when displaying the data
book: Book;
titleInput = '';
statusMessage = '';
  constructor(private service: HttpServiceService) { }

  ngOnInit() {
  }

  searchTitle() {
    this.service.getBookTitle(this.titleInput).subscribe((data: Book[]) => {
      if (data.length > 0) {
        this.books = data;
        this.statusMessage = '';
      } else {
        if (this.books != null) {this.books.length = 0; }
        this.statusMessage = 'No books with that title were found';
      }
    });
  }
}

class Book {
  title: string;
  author: string;
}
