import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {
private url: string = environment.serverUrl + 'api/';
  constructor(private http: HttpClient) { }

  getBookTitle(title) {
    return this.http.get<Bookin[]>(this.url + 'book/' + title);
  }
}

export interface Bookin {
  title: string;
  author: string;
}
